---
title: UVa 10600. ACM contest and Blackout
date: 2018-09-22 12:24:55
tags:
 - UVa
 - Tree
 - LCA
 - MST
categories: 題解
---
{% blockquote %}
# Description
給定一張無向圖，請輸出最小生成樹和次小生成樹的大小。
<!--- more --->
# Input Format
第一行會有t，代表有t個測資$(1 < t < 15)$。
接下來每個測資會有N M，代表n個點以及m條邊(3 < N < 100)。
然後會有M行，$a_i b_i v_i$代表第$i$條邊的兩端點以及權重v。
# Output Format
每組測試資料輸出最小生成樹和次小生成樹大小，中間用一個空格格開。
# Sample Input
{% codeblock line_number:false %}
2
5 8
1 3 75
3 4 51
2 4 19
3 2 95
2 5 42
5 4 31
1 2 9
3 5 66
9 14
1 2 4
1 8 8
2 8 11
3 2 8
8 9 7
8 7 1
7 9 6
9 3 2
3 4 7
3 6 4
7 6 2
4 6 14
4 5 9
5 6 10
{% endcodeblock %}
# Sample Output
{% codeblock line_number:false %}
110 121
37 37
{% endcodeblock %}
UVa - [10600](https://uva.onlinejudge.org/external/106/10600.pdf)  
{% endblockquote %}

# Solution
這題搞了兩天才寫出來，後來是發現原來UVa的毒瘤輸入N會=100，害我一直WA。  
**注意：題目的n範圍應修正為$3 < N \le 100$**  
回歸正題，求次小生成樹的方法是：  
對於已經求好的MST，枚舉每一條未使用的邊，並且判斷加入此邊之後的最小升成樹大小(枚舉的邊必須使用到)。  
也代表假設加入的邊為a, b、長度v，則$原先MST大小-(a到b之間的最大邊長度)+v=此查詢的MST$，然後對每一次查詢的MST求min即可。  

所以重點是要怎麼對每一次查詢求兩點之間的最大邊長，有以下幾種方法可求：
1. 暴力：*$O(N^2)$*預處理，*$O(1)$*查詢
2. LCA倍增法：*$O(NlogN)$*預處理，*$O(logN)$*查詢
3. 樹鏈剖分：*$O(N)$*預處理，*$O(log^2N)$*查詢

這裡我選擇用第二種方法，因為第一種在$n=10^5$的題目上就行不通，而樹鏈剖分常數大且查詢比較慢，實作上也比較難理解。

# Code
```cpp
/*************************************************************************
  > File Name: 10600 - ACM contest and Blackout.cpp
  > Author: Samuel
  > Mail: enminghuang21119@gmail.com
  > Created Time: Thu Sep 20 12:17:39 2018
*************************************************************************/

#include <bits/stdc++.h>
using namespace std;
const int maxn = 110;
int n, m;
struct l {
    int a, b;
    int val;
    bool operator< (const l &b) const {
        return val < b.val;
    }
    l() {}
    l(int a, int b, int v) : a(a), b(b), val(v) {}
};
vector<l> line;

vector<pair<int,int>> mst[maxn];
int parent[maxn], ans1, ans2;

int logn;
vector<int> dp[maxn], maxdp[maxn];
int deep[maxn];

int find(int i) {
    if (i == parent[i])
        return i;
    return parent[i] = find(parent[i]);
}
void MST() {
    for (int i = 0; i <= n; i++) {
        parent[i] = i;
        mst[i].clear();
    }
    int cnt = 0;
    for (auto &i : line) {
        int a = find(i.a);
        int b = find(i.b);
        if (a == b)
            continue;
        parent[b] = a;
        mst[i.a].emplace_back(make_pair(i.b, i.val));
        mst[i.b].emplace_back(make_pair(i.a, i.val));
        ans1 += i.val;
        i.val = 0;
        if (++cnt == n - 1)
            break;
    } 
}
void dfs(int now, int parent, int len) {
    dp[now].resize(logn);
    maxdp[now].resize(logn);
    dp[now][0] = parent;
    maxdp[now][0] = len;
    for (int i = 0; i + 1 < logn; i++) {
        dp[now][i + 1] = dp[dp[now][i]][i];
        maxdp[now][i + 1] = max(maxdp[now][i], maxdp[dp[now][i]][i]);
    }
    for (auto &i : mst[now]) {
        if (i.first == parent)
            continue;
        deep[i.first] = deep[now] + 1;
        dfs(i.first, now, i.second);
    }
}
int LCA(int a, int b) {
    if (deep[a] < deep[b])
        swap(a, b);
    int mx = -1;
    for (int i = deep[a] - deep[b], k = 0; i; i >>= 1, k++)
        if (i & 1) {
            mx = max(mx, maxdp[a][k]);
            a = dp[a][k];
        }
    if (a == b)
        return mx;
    for (int i = logn - 1; i >= 0; i--) {
        if (dp[a][i] != dp[b][i]) {
            mx = max(mx, max(maxdp[a][i], maxdp[b][i]));
            a = dp[a][i];
            b = dp[b][i];
        }
    }
    return max(max(mx, maxdp[a][0]), maxdp[b][0]);
}
int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    int c;
    cin >> c;
    while (c--) {
        cin >> n >> m;
        line.resize(m);
        for (int i = 0; i < m; i++) {
            int a, b, v;
            cin >> a >> b >> v;
            a--; b--;
            line[i] = {a, b, v};
        }
        sort(line.begin(), line.end());
        ans1 = 0;
        ans2 = 1 << 30;
        MST();
        logn = log2(n) + 1;
        dfs(0, 0, 0);
        for (auto &i : line) {
            if (!i.val)
                continue;
            ans2 = min(ans2, i.val - LCA(i.a, i.b));
        }
        cout << ans1 << ' ' << ans1 + ans2 << '\n';
    }
    return 0;
}
```
