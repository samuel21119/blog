---
title: Template. Fibonacci Matrix
date: 2018-08-23 20:41:04
tags:
 - Matrix
 - Fibonacci
categories: 模板
---
$2 \times 2$矩陣表示費氏數列的方法如下  

<!--- more --->
$
\begin{bmatrix}
	x_2 & x_1 \\\\
	1 & 0 \\\\
\end{bmatrix} ^ n
=
\begin{bmatrix}
	F_{n+1} & F_n \\\\
	F_n & F_{n-1} \\\\
\end{bmatrix}
$  

搭配快速冪的算法可以將*$O(N)$*的複雜度降低到*$O(log_2 N)$*。  

程式使用方法：  
`calc(n, a1, a2, x1, x2)`  
表第一項為`a1`，第二項為`a2`；`n`代表要求的第n項。  
且$a_n = x_1 \times a_{n-2} + x_2 \times a_{n-1}$。  

```cpp
struct Matrix{
    int c[2][2];
    Matrix operator*(const Matrix &rhs) const {
        Matrix re;
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 2; j++)
                re.c[i][j] = 0;
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 2; j++)
                for (int k = 0; k < 2; k++)
                    re.c[i][j] += c[i][k] * rhs.c[k][j];
        return re;
	}
};
Matrix Pow(int j, int x1, int x2) {
    Matrix tra;
    tra.c[0][0] = x2; tra.c[0][1] = x1;
    tra.c[1][0] = 1;  tra.c[1][1] = 0;
    Matrix re;
    re.c[0][0] = 1; re.c[0][1] = 0;
    re.c[1][0] = 0; re.c[1][1] = 1;
    for(; j; j >>= 1){
        if (j & 1)
            re = re * tra;
        tra = tra * tra;
    }
    return re;
}
long long int calc(int n, int a1, int a2, int x1, int x2) {
    Matrix a = Pow(n - 1, x1, x2);
    return a.c[1][0] * a2 + a.c[1][1] * a1;
}
```
