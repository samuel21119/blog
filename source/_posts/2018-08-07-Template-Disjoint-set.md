---
title: Template. Disjoint set
date: 2018-08-07 23:34:33
tags: Disjoint set
categories: 模板
---
Disjoint set並查集。
<!--- more --->
# Code
```cpp
#define maxn 1000
struct Disjoint {
    int p[maxn];
    void init() {
        for (int i = 0; i < maxn; i++)
            p[i] = i;
    }
    int parent(int i) {
        if (p[i] == i)
            return i;
        return p[i] = parent(p[i]);
    }
    void connect(int a, int b) {
        p[parent(b)] = parent(a);
    }
    bool same(int a, int b) {
        return(parent(a) == parent(b));
    }
};
```
