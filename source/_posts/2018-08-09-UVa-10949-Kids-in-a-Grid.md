---
title: UVa 10949. Kids in a Grid
date: 2018-08-09 23:45:25
tags:
 - LCS
 - Greedy
 - Binary Search
categories: 題解
---
{% blockquote %}
# Description
兩個小孩在H\*W的格子上走動，每個方格都含有一個字元(ASC碼在33和127間)，這兩個小孩每一步可以走東、西、南、北的任一方向(北是向上，南是向下，東是向右，西是向左)，第一個小孩走了N步，第二個小孩走了M步。$(0 \le N \le M \le 20000)$  
如果我們把他們走過的字母寫成字串，我們可以得到兩串字串$S_A$和$S_B$。你的任務是消掉最少的字元，讓$S_A$和$S_B$變成相等的字串。
<!--- more --->
# Input Format
第一行有一個整數t $(1 \le t \le 15)$，代表有幾組測資，每組測資包含數列。第一列有兩個整數 H 和 W$(1 \le H, W \le 20)$。以下的 H 列為格子上的字元。下一列有3個整數 N、$X_0$、$Y_0$$(1 \le X_0 \le H, 1 \le Y_0 \le W)$，最左上角那格代表(1,1)。第一個小孩從$(X_0,Y_0)$開始走N步，下一列有一串長度為N的字串，N(北)、S(南)、E(東)、W(西)表示他走的方向。第二個小孩走的方法也是類似的。  
你可以假設小孩走的序列是正確的，也就是說絕不會走出格子外。請參考Sample Input。
# Output Format
對每一組測試資料輸出這是第幾組測資和兩個正整數$X_A$和$X_B$，分別是$S_A$和$S_B$需要消掉的最少字元數。
# Sample Input
{% codeblock line_number:false %}
2
3 4
ABCD
DEFG
ABCD
4 1 1
EEES
3 3 1
NES
3 4
ABCD
DEFG
ABCD
4 1 1
EEES
3 3 1
NES
{% endcodeblock %}
# Sample Output
{% codeblock line_number:false %}
Case 1: 3 2
Case 2: 3 2
{% endcodeblock %}
UVa - [10949](https://uva.onlinejudge.org/external/109/10949.pdf)  
翻譯來源：[Luckycat](http://luckycat.kshs.kh.edu.tw/homework/q10949.htm)
{% endblockquote %}
# Solution
這題就是先產生路徑再套LCS，不過需要注意的是 *$O(N^2)$* 方法會TLE，需要改成 *$O(N log_2 N)$*  
[模板庫](http://enminghuang21119.github.io/2018/08/09/Template-Longest-Common-Subsequence/)有較詳細的解說
```cpp
/*************************************************************************
  > File Name: 10949 - Kids in a Grid.cpp
  > Author: Samuel
  > Mail: enminghuang21119@gmail.com
  > Created Time: Thu Aug  9 19:33:40 2018
*************************************************************************/

#include <bits/stdc++.h>
using namespace std;

string road[20];
void input(string &t) {
    int len, x, y;
    char c;
    cin >> len >> x >> y;
    x--; y--;
    t += road[x][y];
    while (len--) {
        cin >> c;
        switch (c) {
            case 'N':
                x--;
                break;
            case 'S':
                x++;
                break;
            case 'W':
                y--;
                break;
            case 'E':
                y++;
        }
        t += road[x][y];
    }
}
int sol(string a, string b) { //a > b
    vector<int> tmp[128];
    for (int i = 0; i < a.length(); i++)
        tmp[a[i]].push_back(i);
    vector<int> lis;
    lis.push_back(-1);
    for (int i = 0; i < b.length(); i++) {
        int now = b[i];
        for (int i = tmp[now].size() - 1; i >= 0; i--) {
            if (tmp[now][i] > lis.back())
                lis.push_back(tmp[now][i]);
            else
                *lower_bound(lis.begin(), lis.end(), tmp[now][i]) = tmp[now][i];
        }
    }
    return lis.size() - 1;
}
int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    int c;
    cin >> c;
    for (int i = 0; i < c;) {
        string a, b;
        int n, m;
        cin >> n >> m;
        for (int i = 0; i < n; i++)
            cin >> road[i];
        input(a);
        input(b);
        int ans = (a.length() > b.length() ? sol(a, b) : sol(b, a));
        cout << "Case " << ++i << ": " << a.length() - ans << ' ' << b.length() - ans << '\n';
    }
    return 0;
}
```
